import { Request, Response, NextFunction } from "express";
import { IRecommendation, IStartSymptom, IQuestion, RecommendationLink } from "../interfaces";
import Recommendation from "../models/Recommendation";
import StartSymptom from "../models/StartSymptom";
import Question from "../models/Question";
import { Types } from "mongoose";


export class PatientController {
    getNextQuestion = async (req: Request, res: Response, next: NextFunction) => {
        let {gender, age, sym_id, answers} = req.query;        

        if (!gender || !age || !sym_id) {
            res.status(422).json({});
        } else {
            const recs:IRecommendation[] = await Recommendation.find({
                genders: { $in: [gender.toString()] },
                ages: { $in: [age.toString()] },
                symptoms: { $in: [Types.ObjectId(sym_id.toString())] }
            });
    
            let qs:IQuestion[] = await Question.find({
                "recommendationLinks.recommendation": {
                    $in: recs.map( rec => rec._id)
                }
            });
    
            // alg
            let recProbs: {[index: string]: any} = setProbabilities(recs);
            qs = sortQuestions(qs, recProbs);
            
            
            if (answers) {
                let patientAnswers:PatientAns[] = JSON.parse(answers.toString());
                if (patientAnswers.length > 0) {
    
                    for (let answer of patientAnswers) {
                        
                        let currQ:IQuestion = qs.filter(q => q._id.toString() === answer.q)[0];                    
                        
                        let relatedRecs:IRecommendation[] = recs.filter((rec) => {
                            return currQ.recommendationLinks.filter(rl => rl.recommendation.toString() === rec._id.toString()).length > 0
                        });                    
                        
                        recProbs = proccessAnswer(answer.ans, currQ, relatedRecs, recProbs);
                        qs.shift();
                        qs = sortQuestions(qs, recProbs);
                    }
                }
            }
            return res.json({
                question: qs.shift(),
                recommendations: sortRecommendations(recs, recProbs).map((rec) => ({title: rec.title, plan: rec.plan, prob: recProbs[rec._id]}))
            })
        }

        
    }

    getStartSymptoms = async (req: Request, res: Response, next: NextFunction) => {
        const resData:IStartSymptom[] = (await Recommendation.find({})
                                        .populate('symptoms'))
                                        .map((rec:IRecommendation) => {
                                            return rec.symptoms
                                        })
                                        .reduce((sym_arr_a, sym_arr_b) => [...sym_arr_a, ...sym_arr_b])
                                        .filter((sym:IStartSymptom, ind:number, self:IStartSymptom[]) => {
                                            return self.indexOf(sym) === ind
                                        }).sort();
        res.json(resData);
    }
}

interface PatientAns {
    q: string;
    ans: number;
}

const sortQuestions = (qs:IQuestion[], recProbs:any):IQuestion[] => {
    const sortFunction = (a:IQuestion, b:IQuestion):number => {
        let aPoints:number = 0; 
        let bPoints:number = 0;

        a.recommendationLinks.forEach((rl) => {
            aPoints += rl.min + rl.max + recProbs[rl.recommendation.toString()]
        });
        b.recommendationLinks.forEach((rl) => {
            bPoints += rl.min + rl.max + recProbs[rl.recommendation.toString()]
        });

        if (aPoints > bPoints) return -1;
        if (aPoints === bPoints) {
            if (a.recommendationLinks.length > b.recommendationLinks.length) return -1;
            if (a.recommendationLinks.length === b.recommendationLinks.length) return 0;
        }
        return 1;
    }
    return qs.sort(sortFunction);
}

const sortRecommendations = (recs:IRecommendation[], recProbs:any):IRecommendation[] => {
    return recs.sort((a:IRecommendation, b:IRecommendation) => {
        return recProbs[b._id] - recProbs[a._id]
    });
}

const setProbabilities = (recs:IRecommendation[]) => {
    let probObj: {[index: string]: any} = {};
    recs.forEach((rec) => {
        probObj[rec._id] = 0.01;
    })
    return probObj;
}

const proccessAnswer = (ans:number, q:IQuestion, recs:IRecommendation[], recProbs:any) => {
    recs.forEach((rec) => {
        const recLink:RecommendationLink = q.recommendationLinks.filter(rl => rl.recommendation.toString() === rec._id.toString())[0];
        if (recLink) {
            const up:number = ((2*recLink.max - 1)*ans/100 + 1 - recLink.max) * recProbs[rec._id];
            const down:number = ((2*recLink.max - 1)*ans/100 + 1 - recLink.max) * recProbs[rec._id] + ((2*recLink.min - 1)*ans/100 + 1 - recLink.min)*(1 - recProbs[rec._id]);
            
            recProbs[rec._id] = down !== 0? up/down : recProbs[rec._id];
        }
    });

    return recProbs;
}