import * as mongoose from 'mongoose';
import {IStartSymptom} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    title: {
        type: String
    },
    type: {
        type: String
    }
});



export default mongoose.model<IStartSymptom>('StartSymptom', schema);
