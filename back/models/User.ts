import * as mongoose from 'mongoose';
import {IBaseUser} from '../interfaces';
import crypto, { Hmac } from "crypto";
import config from '../config';

var debug = require('debug')('get-well-or-die:server');

interface IUser extends mongoose.Model<IBaseUser> {
  authorize(login: string, password: string, callback: void): Promise<void>,
  createNew(login:string, password:string, obj:Object, callback:Function): Promise<void>
}

const schema:mongoose.Schema = new mongoose.Schema({
    login: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
      type: String
    },
    salt: {
      type: String
    },
    created: {
      type: Date,
      default: Date.now
    },
    secondName: {
      type: String,
      required: true
    },
    firstName: {
      type: String,
      required: true
    },
    lastEntered: {
      type: Date,
      default: Date.now
    },
    vkId: {
      type: String
    }
});


schema.virtual('password')
  .set(function (this:IBaseUser, password:string) {

    this._plainPassword = password;

    this.salt = Math.random() + '';
    this.hashedPassword = crypto.createHmac('sha1', this.salt).update(config.appSalt).update(password).digest('hex');
  })
  .get(function (this:IBaseUser) { return this._plainPassword; });


schema.methods.encryptPassword = function (password:string):string {
    return crypto.createHmac('sha1', this.salt).update(config.appSalt).update(password).digest('hex');
};

schema.methods.checkPassword = function(password:string):boolean {
    return this.encryptPassword(password) === this.hashedPassword;
}



export default mongoose.model<IBaseUser, IUser>('BaseUser', schema);
