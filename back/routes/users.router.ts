import { UsersController } from "../controllers/users.controller";
import * as express from 'express';
import { AuthController } from "../controllers/auth.controllers";
import cors from 'cors';

 

// Переписать все с интерфейсами
export default class UserRoutes {
  public usersController: UsersController;
  public authController: AuthController = new AuthController();

  private _router: express.Router;

  constructor() {
      this.usersController = new UsersController();
      this._router = express.Router();
      this._router.use(cors({
          origin: ["http://localhost:3000", "http://192.168.0.116:3000", "http://185.41.161.57", "http://185.41.161.57:8000"],
          credentials: true
      }));
    

      this._initRoutes();
  }

  get router(): express.Router {
    return this._router;
  }

  private _initRoutes(): void {
    
    this._router.get('/get', this.authController.authenticateJWT, this.usersController.get_users);
    // login, password, firstName, secondName
    this._router.post('/sign_up', this.usersController.create_user);
    // login, password
    this._router.post('/login', this.authController.auth);
    //
    this._router.post('/logout', this.authController.logout);
    // 
    this._router.post('/vk_auth', this.authController.oauth);
  }
}
