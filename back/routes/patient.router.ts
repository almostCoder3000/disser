import * as express from 'express';
import cors from 'cors';
import { PatientController } from "../controllers/patient.controller";

 

// Переписать все с интерфейсами
export default class PatientRoutes {
  public patientController: PatientController;

  private _router: express.Router;

  constructor() {
      this.patientController = new PatientController();
      this._router = express.Router();
      this._router.use(cors({
          origin: ["http://localhost:3000", "http://192.168.0.116:3000", "http://185.41.161.57", "http://185.41.161.57:8000"],
          credentials: true
      }));
    

      this._initRoutes();
  }

  get router(): express.Router {
    return this._router;
  }

  private _initRoutes(): void {
    this._router.get('/next_question/get', this.patientController.getNextQuestion);
    this._router.get('/start_symptoms/get', this.patientController.getStartSymptoms);
  }
}