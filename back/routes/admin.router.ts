import * as express from 'express';
import { AuthController } from "../controllers/auth.controllers";
import { AdminController } from '../controllers/admin.controllers';
import cors from 'cors';

// Переписать все с интерфейсами
export default class AdminRoutes {
  public adminController: AdminController;
  public authController: AuthController = new AuthController();

  private _router: express.Router;

  constructor() {
    this.adminController = new AdminController();
    this._router = express.Router();
    this._router.use(cors({
        origin: ["http://localhost:3000", "http://192.168.0.116:3000", "http://185.41.161.57", "http://185.41.161.57:8000"],
        credentials: true
    }));
    this._initRoutes();
  }

  get router(): express.Router {
    return this._router;
  }

  private _initRoutes(): void {
    this._router.get('/recommendation/get', this.authController.authenticateJWT, this.adminController.getRecomendations);
    this._router.post('/recommendation/update', this.authController.authenticateJWT, this.adminController.updateRecomendation);
    this._router.post('/recommendation/remove', this.authController.authenticateJWT, this.adminController.removeRecommendation);
    // title: string, type: BODY_SYMPTOM | OTHER_SYMPTOM
    this._router.post('/symptom/update', this.authController.authenticateJWT, this.adminController.updateStartSymptom);
    this._router.get('/symptom/get', this.authController.authenticateJWT, this.adminController.getStartSymptoms);
    this._router.post('/symptom/remove', this.authController.authenticateJWT, this.adminController.removeStartSymptom);

    this._router.post('/question/update', this.authController.authenticateJWT, this.adminController.updateQuestion);
    this._router.get('/question/get', this.authController.authenticateJWT, this.adminController.getQuestions);
    this._router.post('/question/remove', this.authController.authenticateJWT, this.adminController.removeQuestion);
  }
}
