import * as mongoose from 'mongoose';

export interface IBaseUser extends mongoose.Document {
    login: string,
    hashedPassword: string,
    salt: string,
    created: Date,
    secondName: string,
    firstName: string,
    lastEntered: Date,
    _plainPassword: string,
    vkId: string;
    checkPassword(password: string): boolean,
}

export interface IBlackList extends mongoose.Document {
    token: string;
}

type PlatItem = {name:string, type:string}
export interface IRecommendation extends mongoose.Document {
    title: string;
    ages: string[];
    genders: string[];
    symptoms: IStartSymptom["_id"];
    plan: PlatItem[]
}

const BODY_SYMPTOM = "BODY_SYMPTOM";
const OTHER_SYMPTOM = "OTHER_SYMPTOM";
export type SYMPTOM_TYPE = typeof BODY_SYMPTOM | typeof OTHER_SYMPTOM;

export interface IStartSymptom extends mongoose.Document {
    title: string;
    type: SYMPTOM_TYPE;
}

export type RecommendationLink = {
    recommendation: IRecommendation;
    min: number;
    max: number;
}
export interface IQuestion extends mongoose.Document {
    title: string;
    recommendationLinks: RecommendationLink[];
}