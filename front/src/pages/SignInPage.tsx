import React, { Component, ComponentState } from 'react';
import "../assets/scss/admin.scss";
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { logIn } from '../store/admin/actions';
import { connect } from 'react-redux';
import { AdminState } from '../store/admin/types';
import {ToastsContainer, ToastsStore, ToastsContainerPosition} from 'react-toasts';

interface props extends RouteComponentProps {
    logIn: (login: string, password:string) => Promise<void>;
    admin: AdminState;
}

interface state {
    login: string;
    password: string;
    errors: {
        login: string;
        password: string;
    }
    isFormValid: boolean;
}

class SignInPage extends Component<props, state> {
    state = {
        login: "",
        password: "",
        errors: {
            login: "",
            password: ""
        },
        isFormValid: false
    }

    componentDidUpdate(prevProps:props, preState:state) {
        if (this.props !== prevProps) {
            const {fetching, status, error} = this.props.admin;
            if (status === "authenticated" && !error) {
                this.onAfterSubmit(true);
            } else if (!fetching) {
                
                this.onAfterSubmit(false);
                if (this.props.admin.error === 401) {
                    ToastsStore.error("Ошибка авторизации!")
                } else {
                    ToastsStore.error(`Неизвестная ошибка сервера ${this.props.admin.error}`)
                }
            }
        }
        
    }

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        this.setState(
            {[name] : value} as ComponentState, 
            () => this.validatingForm(name)
        );
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "login":
                await this.setError("login", 
                    this.state.login.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "password":
                await this.setError("password", 
                    this.state.password.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            default:
                break;
        }
    }

    setError = (field: "login" | "password", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    onAfterSubmit = (isOk:boolean) => {
        this.setState({
            login: "",
            password: "",
            errors: {
                login: "",
                password: ""
            },
            isFormValid: false
        }, () => {
            if (isOk) {
                this.props.history.replace("/admin");
            }
        })
    }

    onSubmit = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }
        if (this.state.isFormValid) {
            this.props.logIn(this.state.login, this.state.password);
        }
    }

    render() {
        return (
            <div className="sign-in page">
                <div className="form">
                    <h1>Административная часть</h1>
                    <div className="fields">
                        <div className={`input-wrapper ${this.state.errors.login.length > 0 ? "error" : ""}`}>
                            <input 
                                type="text" 
                                placeholder="Имя пользователя" 
                                name="login"
                                value={this.state.login}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.login}</span>
                        </div>
                        <div className={`input-wrapper ${this.state.errors.password.length > 0 ? "error" : ""}`}>
                            <input 
                                type="password" 
                                placeholder="Пароль" 
                                name="password"
                                value={this.state.password}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.password}</span>
                        </div>
                        <button 
                            className="btn" 
                            data-type="filled"
                            onClick={this.onSubmit}
                        >Войти</button>
                    </div>
                </div>
                <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.BOTTOM_RIGHT}/>
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
    admin: state.admin
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    logIn: async (login: string, password:string) => {
        dispatch(logIn(login, password));
    },    
});


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SignInPage));