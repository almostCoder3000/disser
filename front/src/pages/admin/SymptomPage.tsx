import React, { Component } from 'react';
import Table, { IBasicBodyItem, ITable } from '../../components/admin/Table';
import AdminSlideFormContainer from '../../components/admin/AdminSlideFormContainer';
import StartSymptomForm from '../../components/admin/StartSymptomForm';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { getStartSymptoms, updateStartSymptom, removeStartSymptom } from '../../store/admin/actions';
import { SYMPTOM_TYPE, AdminState, IStartSymptom, RUS_SYMPTOM_TYPES } from '../../store/admin/types';
import { connect } from 'react-redux';


interface props {
    getStartSymptoms: () => Promise<void>;
    removeStartSymptom: (_id:string) => Promise<void>;
    updateStartSymptom: (title: string, type:SYMPTOM_TYPE, _id?:string) => Promise<void>;
    admin: AdminState;
}

interface state {
    table: ITable<IBasicBodyItem>;
}

class SymptomPage extends Component<props, state> {
    state = {
        table: {
            header: [
                {title: "Наименование"},
                {title: "Тип"}
            ],
            body: []
        }
    }

    adminSlideForm:AdminSlideFormContainer | null;
    symptomForm: StartSymptomForm | null;

    componentDidMount() {
        this.props.getStartSymptoms();        
    }

    componentDidUpdate(oldProps:props) {
        if (oldProps.admin.symptoms !== this.props.admin.symptoms)  
            this.setState({
                table: {
                    ...this.state.table,
                    body: this._transformToTableView(this.props.admin.symptoms)
                }
            })
    }

    _transformToTableView(symptoms:IStartSymptom[]):IBasicBodyItem[] {
        return symptoms.map((sym:IStartSymptom, ind: number) => {
            return {
                title: sym.title,
                _id: sym._id,
                otherCells: [
                    {title: RUS_SYMPTOM_TYPES[sym.type], type: "simple", tooltipText: ""}
                ]
            }
        })
    }

    updateAction = (title:string, type: SYMPTOM_TYPE, _id?:string) => {
        this.props.updateStartSymptom(title, type, _id);
        this.adminSlideForm?.closeForm();
    }

    removeAction = (_id:string) => {
        this.props.removeStartSymptom(_id);
        this.adminSlideForm?.closeForm();
    }

    openForm = (_id?:string) => {          
        this.adminSlideForm?.onClickRow();
        this.symptomForm?.onActive(
            this.props.admin.symptoms.filter((item:IStartSymptom) => {
                return item._id === _id
            })[0]
        )
    }

    _renderContent = () => {
        if (this.props.admin.symptoms.length === 0) {
            return (
                <div className="empty-state-container">
                    <h1>Не создано ни одного симптома</h1>
                    <button 
                        className="btn" 
                        onClick={() => {this.openForm()}}
                        data-type="filled"
                    >создать</button>
                </div>
            )
        } else {
            return (
                <>
                <button 
                    className="btn" 
                    onClick={() => {this.openForm()}}
                    data-type="filled"
                >создать симптом</button>
                <div className="table-contaiter" id="style-1">
                    <Table 
                        data={this.state.table} 
                        onClickRow={this.openForm}
                    />
                </div>
                
                </>
            )
        }
    }

    render() {
        return (
            <div className="content">
                <AdminSlideFormContainer ref={node => this.adminSlideForm = node}>
                    <StartSymptomForm 
                        updateAction={this.updateAction}
                        removeAction={this.removeAction}
                        ref={node => this.symptomForm = node}
                    />
                </AdminSlideFormContainer>
                {this._renderContent()}
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
    admin: state.admin
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getStartSymptoms: async () => {
        dispatch(getStartSymptoms());
    },
    updateStartSymptom: async (title: string, type:SYMPTOM_TYPE, _id?:string) => {
        dispatch(updateStartSymptom(title, type, _id));
    },
    removeStartSymptom: async (_id:string) => {
        dispatch(removeStartSymptom( _id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SymptomPage);