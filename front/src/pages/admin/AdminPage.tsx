import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import AdminSwitcher from '../../switchers/AdminSwitcher';
import NavigationBar from '../../components/admin/NavigationBar';
import UserBar from '../../components/admin/UserBar';
import { connect } from 'react-redux';
import { getMe, logout } from '../../store/admin/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../store';
import { IAdmin, StatusAuth } from '../../store/admin/types';

interface props extends RouteComponentProps {
    getMe: () => Promise<void>;
    logout: () => Promise<void>;
    me: IAdmin;
    error?: number;
    fetching: boolean;
    status: StatusAuth
}

interface state {
    
}

class AdminPage extends Component<props, state> {
    componentDidMount() {
        this.props.getMe();        
    }

    componentDidUpdate(oldProps:props) {
        if (this.props.error === 401 || this.props.status === "signin") {
            sessionStorage.removeItem("token");
            this.props.history.replace("/login");
        }
    }

    render() {
        return (
            <div className="page admin">
                <NavigationBar />
                <AdminSwitcher />
                <UserBar
                    lastName={this.props.me.secondName}
                    firstName={this.props.me.firstName}
                    isFetching={this.props.fetching}
                    onLogout={this.props.logout}
                    position="Инженер по знаниям"
                />
            </div>
        )      
    }
}


const mapStateToProps = (state: AppState) => ({
    me: state.admin.me,
    error: state.admin.error,
    fetching: state.admin.fetching,
    status: state.admin.status
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getMe: async () => {
        dispatch(getMe());
    },
    logout: async () => { dispatch(logout()) }
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AdminPage));
