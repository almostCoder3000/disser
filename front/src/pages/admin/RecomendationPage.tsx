import React, { Component } from 'react';
import Table, { ITable, IBasicBodyItem } from '../../components/admin/Table';
import AdminSlideFormContainer from '../../components/admin/AdminSlideFormContainer';
import RecomendationForm from '../../components/admin/RecomendationForm';
import { getStartSymptoms, updateRecommendation, removeRecommendation, getRecommendations } from '../../store/admin/actions';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../store';
import { AdminState, IStartSymptom } from '../../store/admin/types';
import { IRecommendation } from '../../store/admin/types';

interface props {
    getStartSymptoms: () => Promise<void>;
    admin: AdminState;
    updateRecommendation: (rec:IRecommendation) => Promise<void>;
    removeRecommendation: (_id:string) => Promise<void>;
    getRecommendations: () => Promise<void>;
}

interface state {
    table: ITable<IBasicBodyItem>;
}

class RecomendationPage extends Component<props, state> {
    state = {
        table: {
            header: [
                {title: "Наименование"},
                {title: "Первичный симптом"},
                {title: "Пол"}
            ],
            body: []
        }
    }
    adminSlideForm:AdminSlideFormContainer | null;
    recommendationForm: RecomendationForm | null;

    componentDidMount() {
        this.props.getStartSymptoms();
        this.props.getRecommendations();
    }

    componentDidUpdate(oldProps:props) {
        if (oldProps.admin.recommendations !== this.props.admin.recommendations)  
            this.setState({
                table: {
                    ...this.state.table,
                    body: this._transformToTableView(this.props.admin.recommendations)
                }
            })
    }

    _transformToTableView(recs:IRecommendation[]):IBasicBodyItem[] {
        return recs.map((rec:IRecommendation, ind: number) => {            
            return {
                title: rec.title,
                _id: rec._id || "",
                otherCells: [
                    {
                        title: this._getSymptomTitles(rec.symptoms), 
                        type: "simple", 
                        tooltipText: this._getSymptomTitles(rec.symptoms)
                    },
                    {
                        title: this._getGenderTitles(rec.genders as GenderType[]), 
                        type: "simple", 
                        tooltipText: this._getGenderTitles(rec.genders as GenderType[])
                    },
                ]
            }
        })
    }

    _getGenderTitles = (genders:GenderType[]):string => {
        return genders.map((gen:GenderType) => GENDERS[gen]).join(", ");
    }

    _getSymptomTitles = (_ids:string[]):string => {
        if (this.props.admin.symptoms.length > 0) {
            return _ids.map((_id:string) => {
                return this.props.admin.symptoms.filter((sym:IStartSymptom) => {
                    return sym._id === _id
                })[0]
            }).map((sym:IStartSymptom) => sym.title).join(", ");
        } else return ""
        
    }

    openForm = (_id?:string) => {
        this.adminSlideForm?.onClickRow();
        this.recommendationForm?.onActive(
            this.props.admin.recommendations.filter((item:IRecommendation) => {
                return item._id === _id
            })[0]
        )
    }

    _renderContent = () => {
        if (this.props.admin.recommendations.length === 0) {
            return (
                <div className="empty-state-container">
                    <h1>Не создано ни одной рекомендации</h1>
                    <button 
                        className="btn" 
                        onClick={() => {this.openForm()}}
                        data-type="filled"
                    >создать</button>
                </div>
            )
        } else {
            return (
                <>
                <button 
                    className="btn" 
                    onClick={() => {this.openForm()}}
                    data-type="filled"
                >создать рекомендацию</button>
                <div className="table-contaiter" id="style-1">
                    <Table 
                        data={this.state.table} 
                        onClickRow={this.openForm}
                    />
                </div>
                </>
            )
        }
    }

    updateAction = (rec:IRecommendation) => {
        this.props.updateRecommendation(rec)
        this.adminSlideForm?.closeForm();
    }

    removeAction = (_id:string) => {
        this.props.removeRecommendation(_id);
        this.adminSlideForm?.closeForm();
    }

    render() {        
        return (
            <div className="content">
                <AdminSlideFormContainer 
                    ref={node => this.adminSlideForm = node}
                >
                    <RecomendationForm 
                        symptoms={this.props.admin.symptoms}
                        updateForm={this.updateAction}
                        removeAction={this.removeAction}
                        ref={node => this.recommendationForm = node}
                    />
                </AdminSlideFormContainer>
                {this._renderContent()}
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
    admin: state.admin
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getStartSymptoms: async () => {
        dispatch(getStartSymptoms());
    },
    updateRecommendation: async (rec:IRecommendation) => {
        dispatch(updateRecommendation(rec))
    },
    removeRecommendation: async (_id:string) => {
        dispatch(removeRecommendation(_id));
    },
    getRecommendations: async () => {
        dispatch(getRecommendations());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(RecomendationPage);

type GenderType = "man" | "woman";
const GENDERS = {
    man: "Муж.",
    woman: "Жен."
}