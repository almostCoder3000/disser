import React, { Component } from 'react';
import ModalContainer from './ModalContainer';
import { IStartSymptom } from '../../../store/admin/types';
import { IStartAnswer } from '../../../store/patient/types';

interface state {
    title: string;
    description: string;
    symptoms: IStartSymptom[];
    actions: JSX.Element[]
    onSymptomSelect: (ind:number, ans:IStartAnswer) => void;
}

export default class SelectInitialSymptomModal extends Component<{}, state> {
    modal:ModalContainer | null;

    state = {
        title: "",
        description: "",
        symptoms: [],
        actions: [],
        onSymptomSelect: (ind:number, ans:IStartAnswer) => {}
    }

    showModal(
        title:string, description:string, 
        symptoms:IStartSymptom[], onSymptomSelect:(ind:number, ans:IStartAnswer) => void,
        actions: JSX.Element[]
    ) {
        this.modal?.showModal();
        this.setState({ title, description, symptoms, onSymptomSelect, actions });
    }

    _renderSymptoms() {        
        return this.state.symptoms.map((sym:IStartSymptom, ind:number) => {
            return (
                <span key={ind} onClick={() => {
                    this.state.onSymptomSelect(2, {title: sym.title, code: sym._id});
                    this.modal?.hideModal();
                }}>{sym.title}</span>
            )
        })
    }

    render() {
        return (
            <ModalContainer 
                ref={(node) => this.modal = node} 
                className="body-part-modal"
            >
                <h2>{this.state.title}</h2>
                <p>{this.state.description}</p>
                <div className="symptoms">
                    {this._renderSymptoms.bind(this)()}
                </div>
                <div className="actions">
                    {this.state.actions}
                </div>
            </ModalContainer>
        )
    }
}