import React, { Component } from 'react';
import { ITest, IQuestion, IStartAnswer, IAnswerToServer } from '../../store/patient/types';
import SelectInitialSymptomModal from '../common/modals/SelectInitialSymptomModal';
import { IStartSymptom } from '../../store/admin/types';

interface props {
    data?: ITest;
    startSymptoms: IStartSymptom[];
    currentQuestion?: IQuestion;
    createNewTest?: () => void;
    getSymptomById: (sym_id:string) => IStartSymptom;
    updateTest: (test:ITest) => void;
    getNextQuestion: (
        gender: string, age:string, sym_id:string, 
        test_id:string, currentAnswer?: IStartAnswer, 
        answers?:IAnswerToServer[]
    ) => Promise<void>;
}

interface state {
    completedQuestions: IQuestion[];
    currentQuestionInd: number;
    currentQuestion: IQuestion;
}

export default class Test extends Component<props, state> {
    selectInitialSympModal:SelectInitialSymptomModal | null;
    testRef:React.RefObject<HTMLDivElement> = React.createRef();
    reqDataObj: {gender:string, age:string, symptom: string} = {
        gender: "",
        age: "",
        symptom: ""
    };

    state = {
        completedQuestions: [],
        currentQuestionInd: 0,
        currentQuestion: initialQuestions[0]
    }

    componentDidUpdate(prevProps:props) {
        if (this.props !== prevProps) {
            if (this.props.data) {                
                let currentTest:ITest = this.props.data;
                this.setState({
                    completedQuestions: currentTest.completedQuestions,
                    currentQuestion: currentTest.currentQuestion || initialQuestions[0],
                    currentQuestionInd: currentTest.currentQuestionInd
                }, () => {
                    this.testRef.current?.scroll(
                        0,
                        this.testRef.current.scrollHeight
                    )
                });
                this.reqDataObj = {
                    gender: currentTest.gender || "",
                    age: currentTest.age || "",
                    symptom: currentTest.symptom || ""
                }
            }
                
        }
    }

    nextStartQuestion(ind:number, ans:IStartAnswer) {
        let answeredQ:IQuestion = this.state.currentQuestion;
        answeredQ.answer = ans;
        this.setState({
            completedQuestions: [
                ...this.state.completedQuestions, 
                answeredQ
            ],
            currentQuestionInd: this.state.currentQuestionInd + 1
        }, () => {
            this.setState({
                currentQuestion: this._getCurrentQuestion(this.state.currentQuestionInd)
            })
            document.getElementsByClassName("test")[0].scrollTo(
                0, document.getElementsByClassName("test")[0].scrollHeight
            );
        });
        
    }

    openSelectBodyModal() {
        this.selectInitialSympModal?.showModal(
            "Часть тела",
            "Выберите часть тела, в которой вы чувствуете боль.",
            this.props.startSymptoms.filter((ss) => ss.type === "BODY_SYMPTOM"),
            this.nextStartQuestion.bind(this),
            this._renderActions.bind(this)("symptom")
        );
    }

    openSelectAfflictionModal() {
        this.selectInitialSympModal?.showModal(
            "Недуг",
            "Выберите то, что вас беспокоит, не относящееся к части тела.",
            this.props.startSymptoms.filter((ss) => ss.type === "OTHER_SYMPTOM"),
            this.nextStartQuestion.bind(this),
            this._renderActions.bind(this)("symptom")
        );
    }

    nextSystemQuestion = (ansProb:number, startAns:IStartAnswer) => {
        
        this.setState({
            completedQuestions: [
                ...this.state.completedQuestions, 
                {
                    ...this.state.currentQuestion,
                    answer: startAns,
                    answerToServer: {
                        q: this.state.currentQuestion._id || "",
                        ans: ansProb
                    }
                }
            ]
        }, () => {
            if (this.props.data) {
                let answersToServer:IAnswerToServer[] = [];
                let serverQs:IQuestion[] = this.state.completedQuestions.filter((cq:IQuestion) => cq._id);
                serverQs.forEach((sq:IQuestion) => {
                    if (sq.answerToServer) answersToServer.push(sq.answerToServer);
                });
                console.log(this.state.completedQuestions);
                               
                
                this.props.getNextQuestion(
                    this.reqDataObj.gender,
                    this.reqDataObj.age,
                    this.reqDataObj.symptom,
                    this.props.data?.id,
                    startAns,
                    answersToServer
                );
            }
        })
        
        
    }

    _renderActions(type: "gender" | "age" | "symptom" | "other") {
        let buttonProperties:IButtonProperty[] = [];
        switch(type) {
            case "gender":
                buttonProperties.push(
                    {title: "мужской", action: this.nextStartQuestion.bind(this, 0, {title: "мужской", code: "man"})},
                    {title: "женский", action: this.nextStartQuestion.bind(this, 0, {title: "женский", code: "woman"})}
                )
                break;

            case "age":
                [
                    "0-2", "3-6", "7-12", "13-17", "18-24", 
                    "25-34", "35-44", "45-54", "55-64", "65"
                ].forEach((age:string) => {
                    buttonProperties.push(
                        {title: age, action: this.nextStartQuestion.bind(this, 1, {title: age, code: age})},
                    )
                })
                
                break;

            case "symptom":
                buttonProperties.push(
                    {title: "часть тела", action: this.openSelectBodyModal.bind(this)},
                    {title: "недуг", action: this.openSelectAfflictionModal.bind(this)}
                )
                break;

            case "other":
                buttonProperties.push(
                    {title: "нет", action: this.nextSystemQuestion.bind(null, 0, {title: "нет", code: ""})},
                    {title: "скорее нет", action: this.nextSystemQuestion.bind(null, 25, {title: "скорее нет", code: ""})},
                    {title: "не знаю", action: this.nextSystemQuestion.bind(null, 50, {title: "не знаю", code: ""})},
                    {title: "скорее да", action: this.nextSystemQuestion.bind(null, 75, {title: "скорее да", code: ""})},
                    {title: "да", action: this.nextSystemQuestion.bind(null, 100, {title: "да", code: ""})},
                    
                )
                break;

            default:
                break;
        }

        return buttonProperties.map((btn:IButtonProperty, ind:number) => {
            return (
                <button 
                    className={`btn ${type}`} 
                    data-type="ghost"
                    onClick={btn.action}
                    key={ind}
                >{btn.title}</button>
            )
        })

    }

    _renderOldQuestions() {
        return this.state.completedQuestions.map((q:IQuestion, ind: number) => {
            return (
                <div className="questions__item" key={ind}>
                    <h2 className="light">{q.title}</h2>
                    <span>{q.answer?.title || "..."}</span>
                </div>
            )
        })
    }

    _getCurrentQuestion = (ind:number):IQuestion => {
        if (ind < 3) {
            return initialQuestions[ind]
        } else if (ind === 3) {
            let completedQuestions:IQuestion[] = this.state.completedQuestions;
            type SSTypes = "gender" | "age" | "symptom";
            (["gender", "age", "symptom"] as SSTypes[]).forEach((field:SSTypes) => {
                const q:IQuestion = completedQuestions.filter((q:IQuestion) => q.type === field)[0];
                if (q) {
                    if (q.answer) this.reqDataObj[field] = q.answer.code
                }
            });            
            

            if (this.props.data) {
                this.props.getNextQuestion(
                    this.reqDataObj.gender,
                    this.reqDataObj.age,
                    this.reqDataObj.symptom,
                    this.props.data?.id
                );

                const sym:IStartSymptom = this.props.getSymptomById(this.reqDataObj.symptom);
                this.props.updateTest({
                    ...this.props.data,
                    ...this.reqDataObj,
                    title: `${sym.title} ${this.reqDataObj.age}`,
                    completedQuestions: this.state.completedQuestions.map((cq:IQuestion) => ({...cq})),
                    currentQuestionInd: this.state.currentQuestionInd,
                    progress: 30
                });
            }
            
            
            return {
                title: "Подбор подходящего вопроса...",
                answer: null,
                type: "other",
                relatedRecs: []
            }
        } else {
            return {
                title: "Подбор подходящего вопроса...",
                answer: null,
                type: "other",
                relatedRecs: []
            }
        }
    }

    render() {
        if (!this.props.data) {
            return (
                <div className="test" id="style-1">
                    <div className="overflow-wrapper empty">
                        <p>Выберите тест или <br/>
                        <a
                            className="medium"
                            onClick={this.props.createNewTest}
                        >создайте новый</a></p>
                    </div>
                </div>
            )
        } else {            
            let currentQuestion:IQuestion = this.state.currentQuestion;        

            return (
                <div className="test" id="style-1" ref={this.testRef}>
                    <div className="overflow-wrapper">
                        <div className="questions">
                            {this._renderOldQuestions.bind(this)()}
                            <div className="current">
                                <div className="questions__item">
                                    <h2 className="light">{currentQuestion.title}</h2>
                                    <span>...</span>
                                </div>
                                <div className="actions">
                                    {
                                        this.props.data.complete ?
                                        (<div>Тестирование успешно завершено! <br/>Посмотрите свои результаты.</div>) :
                                        this._renderActions.bind(this)(currentQuestion.type)
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <SelectInitialSymptomModal ref={node => this.selectInitialSympModal = node} />
                </div>
            )
        }
        
    }
}


interface IButtonProperty {
    title: string;
    action: () => void;
}

const initialQuestions:IQuestion[] = [
    {title: "Укажите, пожалуйста, Ваш пол", answer: null, type: "gender", relatedRecs: []},
    {title: "Сколько Вам лет?", answer: null, type: "age", relatedRecs: []},
    {title: "Выберите больную часть тела или недуг", answer: null, type: "symptom", relatedRecs: []}
]