import React, { Component } from 'react';
import { ITest } from '../../store/patient/types';

interface props {
    isActive: boolean;
    tests: ITest[];
    onCreateTest: (test:ITest) => void;
    closeAction: () => void;
    onSelectItem: (ind:number) => void;
}

interface state {
    selectedItem: number | null;
}

export default class LeftMenuBoard extends Component<props, state> {
    state = {
        selectedItem: null
    }

    onSelectItem(ind:number) {
        this.setState({selectedItem: ind});
        this.props.onSelectItem(ind);
        this.props.closeAction();
    }

    createTest = () => {
        const currDate:Date = new Date();
        this.props.onCreateTest({
            title: "Новый тест",
            date: currDate.toLocaleDateString(),
            time: currDate.toLocaleTimeString(),
            complete: false,
            currentQuestionInd: 0,
            completedQuestions: [],
            recomendations: [],
            progress: 10,
            id: currDate.toLocaleString()
        });
        this.props.closeAction();
        this.setState({
            selectedItem: 0
        })
    }

    _renderTestItems() {
        if (this.props.tests.length === 0) {
            return (
                <h2 className="light">Создайте свой первый тест!</h2>
            )
        }
        return this.props.tests.map((test:ITest, ind:number) => {
            return (
                <div 
                    className={`test-item ${ind === this.state.selectedItem ? "active" : ""}`} 
                    onClick={this.onSelectItem.bind(this, ind)}
                    key={ind}
                >
                    <div className="gender-circle">{test.gender}</div>
                    <div className="content">
                        <h2 className="regular">{test.title}</h2>
                        <p className="regular">От {test.date} в {test.time}</p>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div className={`left-menu board ${this.props.isActive ? "active" : ""}`}>
                <button onClick={this.createTest} className="btn" data-type="filled">начать тест</button>
                <h1>Тестирования</h1>
                {this._renderTestItems.bind(this)()}
                <button 
                    className="close"
                    onClick={this.props.closeAction}
                ></button>
            </div>
        )
    }
}
