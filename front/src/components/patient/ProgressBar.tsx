import React, { Component } from 'react';

interface props {
    proc: number;
    openInfo: () => void;
}

export default class ProgressBar extends Component<props, {}> {
    render() {
        return (
            <div className="progress">
                <h2>
                    Прогресс тестирования
                    <i onClick={this.props.openInfo}></i>
                </h2>
                <div className="progress__bar">
                    <div 
                        className="progress__bar-fill" 
                        style={{width: `${this.props.proc}%`}}
                    >
                        {this.props.proc}%
                    </div>
                </div>
            </div>
        )
    }
}
