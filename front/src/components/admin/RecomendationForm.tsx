import React, { Component, ComponentState } from 'react';
import { IStartSymptom, PlanItem, PlanItemType, IRecommendation } from '../../store/admin/types';

const formData = {
    ages: [
        {title: "0-2 года", code: "0-2"},
        {title: "3-6 лет", code: "3-6"},
        {title: "7-12 лет", code: "7-12"},
        {title: "13-17 лет", code: "13-17"},
        {title: "18-24 года", code: "18-24"},
        {title: "25-34 года", code: "25-34"},
        {title: "35-44 года", code: "35-44"},
        {title: "45-54 года", code: "45-54"},
        {title: "55-64 года", code: "55-64"},
        {title: "65 и старше", code: "65"},
    ]
}

interface props {
    symptoms: IStartSymptom[];
    updateForm: (rec:IRecommendation) => void;
    removeAction: (_id:string) => void;
}

interface state {
    _id?: string;
    title: string;
    ages: string[];
    genders: string[];
    symptoms: string[];
    errors: {
        title: string;
        newRecItem: string;
        ages: string;
        genders: string;
        symptoms: string;
        plan: string;
    };
    plan: PlanItem[];
    newRecItem: string;
    isFormValid: boolean;
}

const initialState:state = {
    _id: undefined,
    title: "",
    ages: formData.ages.map(age => age.code),
    genders: ["man", "woman"],
    symptoms: [],
    errors: {
        title: "",
        newRecItem: "",
        ages: "",
        genders: "",
        symptoms: "",
        plan: ""
    },
    plan: [],
    newRecItem: "",
    isFormValid: false
}

export default class RecomendationForm extends Component<props, state> {
    state = initialState;

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value, type} = e.target;

        if (type === "text") {
            this.setState(
                {[name] : value} as ComponentState,
                () => this.validatingForm(name)
            );
        } else if (type === "checkbox") {
            let arr:string[] = [];
            
            if (name === "ages") {
                arr = this.state.ages;
            } else if (name === "genders") {
                arr = this.state.genders;
            } else if (name === "symptoms") {
                arr = this.state.symptoms;
            }

            if (arr.indexOf(value) === -1) {
                this.setState(
                    { [name]: [...arr, value] } as ComponentState,
                    () => this.validatingForm(name)
                )
            } else {
                this.setState(
                    { [name]: arr.filter((item:string, ind:number) => {
                        return item !== value
                    }) } as ComponentState,
                    () => this.validatingForm(name)
                )
            }
        }       
    }

    _renderAges = () => {
        return formData.ages.map((age:any, ind:number) => {
            return (
                <label className="chbx-container" key={ind}>
                    {age.title}
                    <input 
                        type="checkbox" 
                        name="ages" 
                        checked={this.state.ages.indexOf(age.code) !== -1}
                        value={age.code} 
                        onChange={this.onHandleChange}
                    />
                    <span className="checkmark"></span>
                </label>
            )
        })
    }

    _renderSymptoms = () => {
        type formSyms = {body: IStartSymptom[], other: IStartSymptom[]}
        let syms:formSyms = {
            body: [],
            other: []
        }
        this.props.symptoms.forEach((sym:IStartSymptom) => {
            syms[sym.type === "BODY_SYMPTOM" ? "body" : "other"].push(sym);
        })
        return (
            <>
            <div>
                {syms.body.map((sym:IStartSymptom, ind:number) => {
                    return (
                        <label className="chbx-container" key={ind}>
                            {sym.title}
                            <input 
                                type="checkbox" 
                                name="symptoms" 
                                checked={this.state.symptoms.indexOf(sym._id) !== -1}
                                value={sym._id} 
                                onChange={this.onHandleChange}
                            />
                            <span className="checkmark"></span>
                        </label>
                    )
                })}
            </div>
            <div>
                {syms.other.map((sym:IStartSymptom, ind:number) => {
                    return (
                        <label className="chbx-container" key={ind}>
                            {sym.title}
                            <input 
                                type="checkbox" 
                                name="symptoms" 
                                checked={this.state.symptoms.indexOf(sym._id) !== -1}
                                value={sym._id}
                                onChange={this.onHandleChange} 
                            />
                            <span className="checkmark"></span>
                        </label>
                    )
                })}
            </div>
            </>
        )
    }

    addRecItem = (type:PlanItemType) => {
        if (this.state.newRecItem.length > 0) {
            this.setState({
                plan: [...this.state.plan, {name: this.state.newRecItem, planType: type}],
                errors: {
                    ...this.state.errors,
                    newRecItem: ""
                },
                newRecItem: ""
            }, () => this.validatingForm("plan"));
        } else {
            this.setState({
                errors: {
                    ...this.state.errors,
                    newRecItem: "Это поле обязательно для заполнения"
                }
            })
        }
        
    }

    removeRecItem = (ind:number) => {
        this.setState({
            plan: this.state.plan.filter((recItem:PlanItem, i:number) => {
                return i !== ind;
            })
        }, () => this.validatingForm("plan"))
    }

    _renderRecItems = () => {
        return this.state.plan.map((recItem:PlanItem, ind:number) => {
            return (
                <div key={ind} className="list-item">
                    <p>{recItem.planType === "doctor" ? "Врач: ": "Анализ: "}{recItem.name}</p>
                    <button onClick={this.removeRecItem.bind(null, ind)} className="btn-close"></button>
                </div>
            )
        })
    }

    setError = (field: "title" | "ages" | "genders" | "symptoms" | "plan", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }


    validatingForm = async (field: string) => {
        switch(field) {
            case "title":
                await this.setError("title", 
                    this.state.title.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "ages":
                await this.setError("ages", 
                    this.state.ages.length === 0 ? "*Выберите хотя бы один вариант" : ""
                );
                break;

            case "genders":
                await this.setError("genders", 
                    this.state.genders.length === 0 ? "*Выберите хотя бы один вариант" : ""
                );
                break;
            
            case "symptoms":
                await this.setError("symptoms", 
                    this.state.symptoms.length === 0 ? "*Выберите хотя бы один вариант" : ""
                );
                break;

            case "plan":
                await this.setError("plan", 
                    this.state.plan.length === 0 ? "*Необходимо добавить хотя бы один пункт рекомендации" : ""
                );
                break;

            default:
                break;
        }
    }

    updateForm = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }

        if (this.state.isFormValid) {
            this.props.updateForm({
                title: this.state.title,
                ages: this.state.ages,
                genders: this.state.genders,
                symptoms: this.state.symptoms,
                plan: this.state.plan,
                _id: this.state._id
            });
            this.setState(initialState);
        }
    }

    onActive = (rec:IRecommendation | undefined) => {
        if (rec) {
            this.setState({
                ...rec
            });
        } else {
            this.setState(initialState);
        }
    }

    removeSymptom = () => {
        if (this.state._id) {
            this.props.removeAction(this.state._id);
            this.setState(initialState);
        }        
    }

    render() {
        return (
            <>
                <h1>{this.state._id ? "Редактирование": "Создание"} рекомендации</h1>
                <div className="fields">
                    <p>
                        Для удобства, можете написать 
                        наименование диагноза, но он не будет показываться пациенту.
                    </p>
                    <div className={`input-wrapper ${this.state.errors.title.length > 0 ? "error" : ""}`}>
                        <input 
                            type="text" 
                            placeholder="Название рекомендации" 
                            name="title"
                            value={this.state.title}
                            onChange={this.onHandleChange}
                        />
                        <span>{this.state.errors.title}</span>
                    </div>
                </div>
                <div className="fields">
                    <p>Выберите возрастную категорию</p>
                    <div className={`checkboxes  ${this.state.errors.ages.length > 0 ? "error" : ""}`}>
                        {this._renderAges()}
                        <span>{this.state.errors.ages}</span>
                    </div>
                </div>
                <div className="fields">
                    <p>Выберите к какому полу или полам может принадлежать эта рекомендация.</p>
                    <div className={`checkboxes  ${this.state.errors.genders.length > 0 ? "error" : ""}`}>
                        <label className="chbx-container">
                            Мужской
                            <input 
                                type="checkbox" 
                                name="genders" 
                                value="man" 
                                checked={this.state.genders.indexOf("man") !== -1}
                                onChange={this.onHandleChange}
                            />
                            <span className="checkmark"></span>
                        </label>
                        <label className="chbx-container">
                            Женский
                            <input 
                                type="checkbox" 
                                name="genders" 
                                value="woman"
                                checked={this.state.genders.indexOf("woman") !== -1}
                                onChange={this.onHandleChange} 
                            />
                            <span className="checkmark"></span>
                        </label>
                        <span>{this.state.errors.genders}</span>
                    </div>
                </div>
                <div className="fields">
                    <p>
                    Выберите часть тела, в которой может быть беспокойство, 
                    или симптом. Это необходимо для более точной классификации 
                    рекомендации.
                    </p>
                    <div className={`checkboxes symptoms  ${this.state.errors.symptoms.length > 0 ? "error" : ""}`}>
                        {this._renderSymptoms()}
                        <span>{this.state.errors.symptoms}</span>
                    </div>
                </div>
                <div className="fields">
                    <p>
                    Выберите врача или анализ в порядке, по которому нужно 
                    проходить рекомендацию.
                    </p>
                    
                    <div className="rec-items">
                        {this._renderRecItems()}
                    </div>
                    <div className="rec-items-form">
                        <div className={`
                            input-wrapper 
                            ${this.state.errors.newRecItem.length > 0 ? "error" : ""}
                            ${this.state.errors.plan.length > 0 ? "error" : ""}
                        `}>
                            <input 
                                type="text" 
                                placeholder="Напишите специальность врача или название анализа" 
                                name="newRecItem"
                                value={this.state.newRecItem}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.newRecItem}</span>
                            <span>{this.state.errors.plan}</span>
                        </div>
                        <div className="actions">
                            <button onClick={this.addRecItem.bind(null, "doctor")} className="btn" data-type="ghost">добавить врача</button>
                            <button onClick={this.addRecItem.bind(null, "analysis")} className="btn" data-type="ghost">добавить анализ</button>
                        </div>
                    </div>
                </div>
                <div className="actions">
                    <button onClick={this.updateForm} data-type="filled" className="btn">сохранить</button>
                    {this.state._id ? (
                        <button onClick={this.removeSymptom} data-type="ghost" className="btn">удалить</button>
                    ) : ""} 
                </div>
            </>
        )
    }
}


