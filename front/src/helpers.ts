import config from "./configure";
import axios from 'axios';

async function postRequest(url:string, data:any) {    
    let headers = {};
    const token:string | null = sessionStorage.getItem("token");    

    if (token) headers = {"Authorization": `Bearer ${token}`};

    return axios({
        method: 'post',
        url: config.urlServer + url,
        headers: headers,
        data: data,
    }).then((res) => {
        return res.data;
    });
}

async function getRequest(url:string, params:any) {    
    let headers = {};
    const token:string | null = sessionStorage.getItem("token");    

    if (token) headers = {"Authorization": `Bearer ${token}`};
    
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach((key:string) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);

    return axios.get(url_fetch, {headers}).then(resp => {
        return resp.data
    })
}


export {postRequest, getRequest};
