export interface IAdmin {
    login: string;
    firstName: string;
    secondName: string;
    _id: string;
}


export type PlanItemType = "doctor" | "analysis";
export type PlanItem = {name:string, planType:PlanItemType}
export interface IRecommendation {
    title: string;
    ages: string[];
    genders: string[];
    symptoms: string[];
    plan: PlanItem[];
    _id?: string;
}

export interface IRecommendationLink {
    recommendation: IRecommendation;
    max: number;
    min: number;
}
export interface IQuestion {
    _id?: string;
    title: string;
    recommendationLinks: IRecommendationLink[];
}

export type SYMPTOM_TYPE = "BODY_SYMPTOM" | "OTHER_SYMPTOM";

export const RUS_SYMPTOM_TYPES = {
    "BODY_SYMPTOM": "Часть тела",
    "OTHER_SYMPTOM": "Другое"
}


export interface IStartSymptom {
    _id: string;
    title: string;
    type: SYMPTOM_TYPE;
}

export type StatusAuth = "authenticated" | "signin" | "storage" | "logout" | null;

export interface AdminState {
    fetching: boolean;
    me: IAdmin;
    status: StatusAuth;
    symptoms: IStartSymptom[];
    recommendations: IRecommendation[];
    questions: IQuestion[];
    error?: number;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";
export const AUTH = "AUTH";

// success
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const GET_ME_SUCCESS = "GET_ME_SUCCESS";
export const LOGOUT = "LOGOUT";

export const GET_SYMPTOMS = "GET_SYMPTOMS";
export const UPDATE_SYMPTOM = "UPDATE_SYMPTOM";
export const REMOVE_SYMPTOM = "REMOVE_SYMPTOM";

export const RESET_REGISTER_FLAG = "RESET_REGISTER_FLAG";

export const GET_RECOMMENDATIONS = "GET_RECOMMENDATIONS";
export const UPDATE_RECOMMENDATION = "UPDATE_RECOMMENDATION";
export const REMOVE_RECOMMENDATION = "REMOVE_RECOMMENDATION";

export const GET_QUESTIONS = "GET_QUESTIONS";
export const UPDATE_QUESTION = "UPDATE_QUESTION";
export const REMOVE_QUESTION = "REMOVE_QUESTION";

// error
export const AUTH_ERROR = "AUTH_ERROR";


export interface StartFetchAction {
    type: typeof START_FETCH;
}

export interface AuthAction {
    type: typeof AUTH;
}

export interface AuthErrorAction {
    type: typeof AUTH_ERROR;
    payload: number;
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: number;
}


interface AuthActionSuccess {
    type: typeof AUTH_SUCCESS;
    payload: IAdmin;
}
interface GetMeSuccess {
    type: typeof GET_ME_SUCCESS;
    payload: IAdmin;
}
interface GetSymptoms {
    type: typeof GET_SYMPTOMS;
    payload: IStartSymptom[];
}
interface UpdateSymptom {
    type: typeof UPDATE_SYMPTOM;
    payload: IStartSymptom[];
}
interface RemoveSymptom {
    type: typeof REMOVE_SYMPTOM;
    payload: IStartSymptom[];
}
interface Logout {
    type: typeof LOGOUT;
}
interface getRecommendations {
    type: typeof GET_RECOMMENDATIONS;
    payload: IRecommendation[];
}
interface updateRecommendation {
    type: typeof UPDATE_RECOMMENDATION;
    payload: IRecommendation[];
}
interface removeRecommendation {
    type: typeof REMOVE_RECOMMENDATION;
    payload: IRecommendation[];
}

interface getQuestions {
    type: typeof GET_QUESTIONS;
    payload: IQuestion[];
}
interface updateQuestion {
    type: typeof UPDATE_QUESTION;
    payload: IQuestion[];
}
interface removeQuestion {
    type: typeof REMOVE_QUESTION;
    payload: IQuestion[];
}


export type AdminActionTypes = StartFetchAction 
                              | FetchActionError
                              | AuthAction
                              | GetMeSuccess
                              | AuthActionSuccess
                              | AuthErrorAction
                              | GetSymptoms
                              | UpdateSymptom
                              | RemoveSymptom
                              | Logout
                              | getRecommendations
                              | updateRecommendation
                              | removeRecommendation
                              | getQuestions
                              | updateQuestion
                              | removeQuestion;
