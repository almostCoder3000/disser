import React from 'react'
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom'
import RecomendationPage from '../pages/admin/RecomendationPage';
import QuestionPage from '../pages/admin/QuestionPage';
import SymptomPage from '../pages/admin/SymptomPage';
import ReviewPage from '../pages/admin/ReviewPage';

interface props {
    
}

export default function AdminSwitcher(props:props) {
    let match = useRouteMatch();
    return (
        <Switch>
            <Route path={`${match.path}/recomendations`}>
                <RecomendationPage />
            </Route>
            <Route path={`${match.path}/questions`}>
                <QuestionPage />
            </Route>
            <Route path={`${match.path}/symptoms`}>
                <SymptomPage />
            </Route>
            <Route path={`${match.path}/reviews`}>
                <ReviewPage />
            </Route>
            <Route path={`${match.path}`}>
                <Redirect to={`${match.path}/recomendations`} />
            </Route>
        </Switch>
    )
}
