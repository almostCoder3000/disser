import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import MainPage from '../pages/patient/MainPage';
import SignInPage from '../pages/SignInPage';
import AdminPage from '../pages/admin/AdminPage';

export default class MainSwitcher extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/">
                    <MainPage />
                </Route>
                <Route path="/login">
                    <SignInPage />
                </Route>
                <Route path="/admin">
                    <AdminPage />
                </Route>
            </Switch>
        )
    }
}
